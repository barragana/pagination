import React from 'react';
import {
  renderIntoDocument,
  scryRenderedDOMComponentsWithClass,
  Simulate,
  findRenderedDOMComponentWithTag
} from 'react-addons-test-utils';
import {expect} from 'chai';
import {Dropdown} from '../../src/components/Dropdown';

describe('Dropdown', () => {

  let label;
  let options;
  let component;
  let callback = (text) => text;

  beforeEach(() => {
    label = 'Gender';
    options = ['Male', 'Female'];
    component = renderIntoDocument(
      <Dropdown options={options} label={label} handleSelectedItem={callback}/>
    );
  })

  it('renders options with none selected gender', () => {
    const items = findRenderedDOMComponentWithTag(component, 'span');
    const [male, female] = entries.map(e => e.textContent);
    expect(items.length).to.equal(2);
    expect(male).to.contain('Trainspotting');
    expect(female).to.contain('28 Days Later');
  });

});
