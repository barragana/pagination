import {expect} from 'chai';
// import {  } from 'lodash/object';
import { map, mapValues } from 'lodash';

import { default as reducer } from '../../src/reducers/friendlist';

describe('reducer', () => {
  let initialState;

  beforeEach (() =>{
    initialState = {
      friendsById: {
        0: {
          id: 3,
          name: 'Theodore Roosevelt',
          gender: 'Male'
        },
        1: {
          id: 2,
          name: 'Fabi Lincoln',
          gender: 'Female'
        },
        2: {
          id: 1,
          name: 'George Washington',
          gender: 'Male'
        }
      },
      pagination: {
        page: 0,
        more: 2,
        limit: 2
      }
    };
  })

  it('handles CHANGE_GENDER', () => {

    const action = {
      type: 'CHANGE_GENDER',
      id: 2,
      gender: 'Male'
    };

    let expectedNextState = { ...initialState };
    expectedNextState.friendsById[1] = {
      id: 2,
      name: 'Fabi Lincoln',
      gender: 'Male'
    };

    const nextState = reducer(initialState, action);

    expect(nextState).to.deep.equal(expectedNextState);
  });

  it('handles ADD_FRIEND', () => {

    const action = {
      type: 'ADD_FRIEND',
      name: 'Mateus'
    };

    let expectedNextState = { ...initialState };
    expectedNextState.friendsById = map(expectedNextState.friendsById);
    expectedNextState.friendsById.unshift({name: 'Mateus', id: 4});
    expectedNextState.friendsById = mapValues(expectedNextState.friendsById);

    const nextState = reducer(initialState, action);

    expect(nextState).to.deep.equal(expectedNextState);
  });

  it('handles DELETE_FRIEND', () => {

    const action = {
      type: 'DELETE_FRIEND',
      id: '3'
    };

    initialState.pagination.limit = 4

    let expectedNextState = { ...initialState };
    expectedNextState.pagination.limit = 2

    const nextState = reducer(initialState, action);

    expect(nextState).to.deep.equal(expectedNextState);
  });

  it('handles MORE_FRIENDS', () => {

    const action = {
      type: 'MORE_FRIENDS',
    };

    let expectedNextState = { ...initialState };
    expectedNextState.pagination = {
      page: 0,
      more: 2,
      limit: 4
    }

    const nextState = reducer(initialState, action);

    expect(nextState).to.deep.equal(expectedNextState);
  });
});
