import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import styles from './FriendListApp.css';

import * as FriendsActions from '../actions/FriendsActions';
import {
  FriendList,
  AddFriendInput,
  Pagination
} from '../components';

@connect(state => ({
  friendlist: state.friendlist
}))

export default class FriendListApp extends Component {

  static propTypes = {
    friendlist: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  render () {
    const { friendlist, dispatch } = this.props;
    const actions = bindActionCreators(FriendsActions, dispatch);
    const data = {
      list: friendlist.friendsById,
      pagination: {...friendlist.pagination}
    };

    return (
      <div className={styles.friendListApp}>
        <h1>The FriendList</h1>
        <AddFriendInput addFriend={actions.addFriend} />
        <Pagination data={data} showMore={actions.moreFriends}>
          <FriendList list={friendlist.friendsById} actions={actions} />
        </Pagination>
      </div>
    );
  }
}
