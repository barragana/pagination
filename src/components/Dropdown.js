import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import style from './Dropdown.css';

export default class Dropdown extends Component {

  constructor(props) {
    super(props);
    this.handleSelectedItem = this.handleSelectedItem.bind(this);
    this.handleOpenDropdown = this.handleOpenDropdown.bind(this);
  }

  static propTypes = {
    handleSelectedItem: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.string),
    label: PropTypes.string.isRequired
  }

  state = {dropdown: 'dropdown'}

  handleSelectedItem (e) {
    this.setState({dropdown: 'dropdown'});
    this.props.handleSelectedItem(
      (e.target.type !== 'button' && e.target.innerText) || this.props.label);
    e.stopPropagation();
  }

  handleOpenDropdown () {
    this.setState({dropdown: 'dropdown open'});
  }

  render () {
    return (
      <button type="button" className={style.dropdownButton}
      onBlur={this.handleSelectedItem.bind(this)}>
        <div
          className={classnames(this.state.dropdown, style.overrideDropdown)}
          onClick={this.handleOpenDropdown}
        >
          <small>{this.props.label} </small>
          <span className="caret"></span>
          <ul
            className={classnames('dropdown-menu', style.overrideMenu)}
            onClick={this.handleSelectedItem}
          >
            { this.props.options.map(option => <li><small name={option}>{option}</small></li>) }
          </ul>
        </div>
      </button>
    );
  }
}
