import React, { Component, PropTypes } from 'react';
import { size } from 'lodash';

import styles from './Pagination.css';

export default class Pagination extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    data: PropTypes.shape({
      pagination: PropTypes.shape({
        page: PropTypes.number.isRequired,
        limit: PropTypes.number.isRequired
      }).isRequired,
      list: PropTypes.object.isRequired
    }).isRequired
  }

  render () {
    var { data, children, showMore } = this.props;
    var node = React.cloneElement(children, {
      list: paginate(data),
      actions: children.props.actions
    });

    const { pagination: { limit }, list } = data;

    return (<div>
      {node}
      { limit < size(list) ? <div className={styles.pagination} onClick={showMore}>
        <i className="fa fa-angle-down fa-2x" aria-hidden="true"></i>
      </div> : null}
    </div>);
  }
}

const paginate = ({ list, pagination: { page, limit } }) => {
  return _(list)
    .sortBy()
    .slice(page, limit)
    .mapValues()
    .value();
}
