import * as types from '../constants/ActionTypes';

export function addFriend(name) {
  return {
    type: types.ADD_FRIEND,
    name
  };
}

export function changeGender({id, gender}) {
  return {
    type: types.CHANGE_GENDER,
    id: id,
    gender: gender
  };
}

export function deleteFriend(id) {
  return {
    type: types.DELETE_FRIEND,
    id
  };
}

export function starFriend(id) {
  return {
    type: types.STAR_FRIEND,
    id
  };
}

export function refreshFriends() {
  return {
    type: types.REFRESH_FRIENDS
  };
}

export function moreFriends() {
  return {
    type: types.MORE_FRIENDS
  };
}
