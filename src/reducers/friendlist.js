import * as types from '../constants/ActionTypes';
import {omit, assign, merge, mapValues} from 'lodash/object';
import _, {isEmpty, map, size, first } from 'lodash';

const initialState = {
  friendsById: {
    0: {
      id: 3,
      name: 'Theodore Roosevelt',
      gender: 'Male'
    },
    1: {
      id: 2,
      name: 'Fabi Lincoln',
      gender: 'Female'
    },
    2: {
      id: 1,
      name: 'George Washington',
      gender: 'Male'
    }
  },
  pagination: {
    page: 0,
    more: 2,
    limit: 2
  }
};

export function pagination(state, action) {
  switch (action.type) {
    case types.MORE_FRIENDS:
      return  {
        ...state,
        pagination: {
          ...state.pagination,
          limit: state.pagination.limit + state.pagination.more
        }
      };
  }
}

export function friends(state, action) {
  switch (action.type) {
    case types.ADD_FRIEND:
      const firstFriend = first(state.friendsById);
      const newId = (!isEmpty(firstFriend) && firstFriend.id + 1) || 0;
      const list = map(state.friendsById) || [];
      list.unshift({id: newId, name: action.name});
      return {
        ...state,
        friendsById: mapValues(list)
      }

    case types.DELETE_FRIEND:
      var list = omit(state.friendsById, friend => friend.id === action.id);
      return {
        ...state,
        friendsById: list,
        pagination: {
          ...state.pagination,
          limit: size(list) < state.pagination.limit && size(list) % 2 === 0 ?
            Math.max(state.pagination.limit - state.pagination.more, state.pagination.more) :
            state.pagination.limit
        }
      }

    case types.CHANGE_GENDER:
      return {
        ...state,
        friendsById: mapValues(state.friendsById, (friend) => {
          return friend.id === action.id ?
            assign({}, friend, { gender: action.gender }) :
            friend
        })
      }

    case types.STAR_FRIEND:
      return {
        ...state,
        friendsById: mapValues(state.friendsById, (friend) => {
          return friend.id === action.id ?
            assign({}, friend, { starred: !friend.starred }) :
            friend
        })
      }
  }
}

export default function friendlist(state = initialState, action) {
  const nextState = merge({},
    pagination(state, action),
    friends(state, action));

  return isEmpty(nextState) ? state : nextState;
}
